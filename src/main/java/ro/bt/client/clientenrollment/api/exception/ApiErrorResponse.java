package ro.bt.client.clientenrollment.api.exception;

public class ApiErrorResponse {

    private boolean succes;
    private String message;

    public ApiErrorResponse(String message) {
        this.succes = false;
        this.message = message;
    }

    public static ApiErrorResponse build(String message) {
        return new ApiErrorResponse(message);
    }

    public boolean isSucces() {
        return succes;
    }

    public void setSucces(boolean succes) {
        this.succes = succes;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
}

