package ro.bt.client.clientenrollment.api.exception;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

@RestControllerAdvice
public class RestGlobalExceptionHandler extends ResponseEntityExceptionHandler {

    private static final Logger LOGGER = LoggerFactory.getLogger(RestGlobalExceptionHandler.class);

    @ExceptionHandler(BadRequestException.class)
    public ResponseEntity handlebadRequest(BadRequestException e) {
        LOGGER.warn("BadRequestException with stacktrace: ", e);
        return ResponseEntity.badRequest().body(ApiErrorResponse.build(e.getMessage()));
    }
}
