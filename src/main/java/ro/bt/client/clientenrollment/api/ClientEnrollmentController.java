package ro.bt.client.clientenrollment.api;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import ro.bt.client.clientenrollment.api.dto.ClientDataDto;
import ro.bt.client.clientenrollment.service.ClientEnrollmentService;
import ro.bt.client.clientenrollment.service.DocumentService;

import javax.validation.Valid;

@RestController
@RequestMapping("api/v1/clients")
public class ClientEnrollmentController {

    @Autowired
    private ClientEnrollmentService clientEnrollmentService;

    @Autowired
    private DocumentService documentService;

    /**
     * Endpoint for perofming a full client check
     * Bad requests are handled globally by {@link ro.bt.client.clientenrollment.api.exception.RestGlobalExceptionHandler}
     * @param clientDataDto {@link ClientDataDto}
     * @return A message with the respnse after the cueck is performed
     */
    @PostMapping()
    public ResponseEntity<?> fullClientCheck(@Valid @RequestBody ClientDataDto clientDataDto) {
            return ResponseEntity.ok(clientEnrollmentService.fullClientCheck(clientDataDto));
    }

    /**
     * Endpoint for generating an acceptance document
     * @param clientDataDto {@link ClientDataDto}
     * @return the acceptance document
     */
    @PostMapping("/acceptance")
    public ResponseEntity<?> getAcceptanceDocument(@Valid @RequestBody ClientDataDto clientDataDto) {
        return ResponseEntity.ok(documentService.generateDocument("ACCEPTANCE",clientDataDto));
    }

    /**
     * Endpoint for generating a rejection document
     * @param clientDataDto {@link ClientDataDto}
     * @return the rejection document
     */
    @PostMapping("/rejection")
    public ResponseEntity<?> getRejectionDocument(@Valid @RequestBody ClientDataDto clientDataDto) {
        return ResponseEntity.ok(documentService.generateDocument("REJECTION",clientDataDto));
    }

}
