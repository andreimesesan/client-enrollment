package ro.bt.client.clientenrollment.model;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.time.LocalDate;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class ClientData {

    private String firstName;
    private String lastName;
    private LocalDate validFrom;
    private LocalDate validTo;
    private String CNP;
    private String series;
    private String number;

    @Override
    public String toString() {
        return "ClientData{" +
                "firstName='" + firstName + '\'' +
                ", lastName='" + lastName + '\'' +
                ", validFrom=" + validFrom +
                ", validTo=" + validTo +
                ", CNP='" + CNP + '\'' +
                ", series='" + series + '\'' +
                ", number='" + number + '\'' +
                '}';
    }
}
