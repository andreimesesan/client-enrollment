package ro.bt.client.clientenrollment.service;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import ro.bt.client.clientenrollment.model.ClientSearchCriteria;
import ro.bt.client.clientenrollment.util.ServiceConnector;

@Component
public class ClientExistenceCheckService {

    private static final Logger LOGGER = LoggerFactory.getLogger(ClientExistenceCheckService.class);

    //Provisional URL, just for test only, to be changed when real system provided
    private static final String URL = "http://localhost:7070/api/v1/clients/check";

    @Autowired
    private ServiceConnector serviceConnector;

    /**
     * Method which calls the client existence system
     * @param CNP, client CNP
     * @param series, client ID series
     * @param number, client ID number
     * @return whether the client exists
     */
    Boolean checkIfClientExists(final String CNP, final String series,final String number) throws Exception {
        ClientSearchCriteria clientSearchCriteria = new ClientSearchCriteria(number,CNP,series);
        LOGGER.info("Checking if client {} {} {} exists",CNP, series,number);
        return Boolean.parseBoolean(serviceConnector.getResponseFromSystem(URL,clientSearchCriteria));
    }
}
