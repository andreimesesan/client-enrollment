package ro.bt.client.clientenrollment.service;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ro.bt.client.clientenrollment.model.ClientSearchCriteria;
import ro.bt.client.clientenrollment.util.ServiceConnector;

@Service
public class ClientReputationService {

    private static final Logger LOGGER = LoggerFactory.getLogger(ClientReputationService.class);

    //Provisional URL, just for test only, to be changed when real system provided
    private static final String URL = "SOME URL TO CHECK/api/score";

    @Autowired
    private ServiceConnector serviceConnector;

    /**
     * Method which calls the client score system
     * @param CNP, client CNP
     * @param series, client ID series
     * @param number, client ID number
     * @return the client score
     */
    Integer getClientReputationScore(String CNP, String series, String number) throws Exception {
        ClientSearchCriteria clientSearchCriteria = new ClientSearchCriteria(number,CNP,series);
        LOGGER.info("Checking client reputation for: {}",clientSearchCriteria.toString());
        return Integer.parseInt(serviceConnector.getResponseFromSystem(URL,clientSearchCriteria));
    }
}
