package ro.bt.client.clientenrollment.service;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ro.bt.client.clientenrollment.api.dto.ClientDataDto;
import ro.bt.client.clientenrollment.document.DocumentFactory;
import ro.bt.client.clientenrollment.model.ClientData;
import ro.bt.client.clientenrollment.util.ClientDtoConverter;

@Service
public class DocumentService {

    private static final Logger LOGGER = LoggerFactory.getLogger(DocumentService.class);

    @Autowired
    private DocumentFactory documentFactory;

    public byte [] generateDocument(final String decision, final ClientDataDto clientDataDto) {
        ClientData clientData = ClientDtoConverter.convertFromDto(clientDataDto);
        LOGGER.info("Generating document for client {}, with decision {}", clientDataDto.getCNP(), decision);
        return documentFactory.getDocument(decision).generate(clientData);
    }
}
