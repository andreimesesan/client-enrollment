package ro.bt.client.clientenrollment.service;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ro.bt.client.clientenrollment.api.dto.ClientDataDto;

import java.time.LocalDate;

@Service
public class ClientEnrollmentService {

    private static final Logger LOGGER = LoggerFactory.getLogger(ClientEnrollmentService.class);

    @Autowired
    private ClientExistenceCheckService clientExistenceCheckService;

    @Autowired
    private ClientReputationService clientReputationService;

    /**
     * Method which handles the client check
     *
     * @param clientDataDto{@link ro.bt.client.clientenrollment.api.dto.ClientDataDto}
     * @return String message containing information about the check, or exception message if there was an error during the service calls
     */

    public String fullClientCheck(final ClientDataDto clientDataDto) {
        LOGGER.info("ANALYZING CLIENT INFO...");
        final Boolean clientExists;
        final Integer score;
        final Boolean validId;
        try {
            clientExists = clientExistenceCheckService.checkIfClientExists(clientDataDto.getCNP(), clientDataDto.getSeries(), clientDataDto.getNumber());
            score = clientReputationService.getClientReputationScore(clientDataDto.getCNP(), clientDataDto.getSeries(), clientDataDto.getNumber());
            validId = isValidId(clientDataDto);

            return acceptanceDecision(clientExists, score, validId);

        } catch (Exception e) {
            LOGGER.warn("EXCEPTION DURING CLIENT CHECK: {}", e.getMessage());
            e.printStackTrace();
        }
        return "Exception during client check.";
    }

    /**
     * Method which handles the acceptance decision
     *
     * @param clientExists, boolean response from {@link ClientExistenceCheckService} existence check call
     * @param score,        integer, response form {@link ClientReputationService} score api call
     * @param validId,      boolean value represents whether id is expired or not
     * @return a message representing the client status based on the three params
     */
    private String acceptanceDecision(final Boolean clientExists, final Integer score, final Boolean validId) {
        if (clientExists) {
            return "Client already enrolled!";
        } else if (!validId) {
            return "Expired ID, enrollment not possible!";
        } else {
            return getClientReputationBasedOnScore(score);
        }
    }

    /**
     * Method which handles the candidate score
     * @param score, integer, response form {@link ClientReputationService} score api call
     * @return A message based on the score
     */
    private String getClientReputationBasedOnScore(final Integer score) {
        if (score >= 0 && score <= 20) {
            return "Candidate with no risk";
        } else if (score <= 99) {
            return "Candidate with medium risk, but enrollment still possible";
        } else {
            return "Risky candidate, enrollment not acceptable";
        }
    }


    private Boolean isValidId(final ClientDataDto clientDataDto) {
        return LocalDate.now().isBefore(LocalDate.parse(clientDataDto.getValidTo()));
    }

}
