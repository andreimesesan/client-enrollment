package ro.bt.client.clientenrollment.document;

import ro.bt.client.clientenrollment.model.ClientData;

public interface Document {

    byte [] generate(ClientData clientData);
}
