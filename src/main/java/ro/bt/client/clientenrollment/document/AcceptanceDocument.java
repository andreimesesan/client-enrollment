package ro.bt.client.clientenrollment.document;

import ro.bt.client.clientenrollment.model.ClientData;

import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;

public class AcceptanceDocument implements Document {
    @Override
    public byte[] generate(final ClientData clientData) {
        //TEMPORARY MOCK
        Charset charset = StandardCharsets.UTF_8;
        return "ACCEPTANCE_DOCUMENT".getBytes(charset);
//        return new byte[0];
    }
}
