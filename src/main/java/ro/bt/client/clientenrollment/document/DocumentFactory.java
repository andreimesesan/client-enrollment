package ro.bt.client.clientenrollment.document;

import org.springframework.stereotype.Component;

@Component
public class DocumentFactory {

    public Document getDocument(final String decision) {
        if("ACCEPTANCE".equals(decision)) {
            return new AcceptanceDocument();
        } else {
            return new RejectionDocument();
        }
    }
}
