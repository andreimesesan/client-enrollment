package ro.bt.client.clientenrollment.util;

import ro.bt.client.clientenrollment.api.dto.ClientDataDto;
import ro.bt.client.clientenrollment.model.ClientData;

import java.time.LocalDate;

public final class ClientDtoConverter {

    public static ClientData convertFromDto(ClientDataDto clientDataDto){
        ClientData clientData = new ClientData();

        clientData.setFirstName(clientDataDto.getFirstName());
        clientData.setLastName(clientDataDto.getLastName());
        clientData.setCNP(clientDataDto.getCNP());
        clientData.setSeries(clientDataDto.getSeries());
        clientData.setNumber(clientDataDto.getNumber());
        clientData.setValidFrom(LocalDate.parse(clientDataDto.getValidFrom()));
        clientData.setValidTo(LocalDate.parse(clientDataDto.getValidTo()));

        return clientData;
    }
}
