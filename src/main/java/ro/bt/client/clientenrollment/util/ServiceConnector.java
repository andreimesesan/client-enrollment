package ro.bt.client.clientenrollment.util;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;
import ro.bt.client.clientenrollment.model.ClientSearchCriteria;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.nio.charset.StandardCharsets;
import java.util.HashMap;
import java.util.Map;


@Component
public class ServiceConnector {
    /**
     * Class which handles the calls to an external system.
     */
    private static final Logger LOGGER = LoggerFactory.getLogger(ServiceConnector.class);

    /**
     * Method which creates a POST HTTP Call
     * @param URL, URL of external system
     * @param clientSearchCriteria, search params for the external system
     * @return The response string from the API call
     */
    public String getResponseFromSystem(final String URL, final ClientSearchCriteria clientSearchCriteria) throws Exception {
        URL url = new URL(URL);
        HttpURLConnection con = (HttpURLConnection) url.openConnection();
        //create request header
        con.setRequestMethod("POST");
        con.setRequestProperty("Content-Type", "application/json; utf-8");
        con.setRequestProperty("Accept", "application/json");
        con.setDoOutput(true);

        //create request body
        Map<String, String> parameters = new HashMap<>();
        parameters.put("CNP", clientSearchCriteria.getCNP());
        parameters.put("series", clientSearchCriteria.getSeries());
        parameters.put("number", clientSearchCriteria.getCNP());

        LOGGER.info("Creating HTTP POST request...");

        String json = new ObjectMapper().writeValueAsString(parameters);
        try(OutputStream os = con.getOutputStream()) {
            byte[] input = json.getBytes(StandardCharsets.UTF_8);
            os.write(input, 0, input.length);
        }

        LOGGER.info("Sending parameters...");

        try(BufferedReader br = new BufferedReader(
                new InputStreamReader(con.getInputStream(), StandardCharsets.UTF_8))) {
            StringBuilder response = new StringBuilder();
            String responseLine;
            while ((responseLine = br.readLine()) != null) {
                response.append(responseLine.trim());
            }
            return response.toString();
        }
    }
}
