package ro.bt.client.clientenrollment.util;

public enum Decision {
    ACCEPTANCE, REJECTION;
}
