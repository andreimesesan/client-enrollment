package ro.bt.client.clientenrollment;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ClientEnrollmentApplication {

	public static void main(String[] args) {
		SpringApplication.run(ClientEnrollmentApplication.class, args);
	}

}
