package ro.bt.client.clientenrollment.service;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;
import ro.bt.client.clientenrollment.api.dto.ClientDataDto;

import static org.junit.jupiter.api.Assertions.assertEquals;

@SpringBootTest
@ActiveProfiles("test")
public class ClientEnrollmentServiceTest {

    @Mock
    private ClientExistenceCheckService clientExistenceCheckService;

    @Mock
    private ClientReputationService clientReputationService;

    @InjectMocks
    private ClientEnrollmentService clientEnrollmentService = new ClientEnrollmentService();

    private ClientDataDto clientDataDto1;
    private ClientDataDto clientDataDto2;

    @BeforeEach
    private void setUp() {
        clientDataDto1 = new ClientDataDto();
        clientDataDto1.setCNP("12345678911111");
        clientDataDto1.setNumber("123456");
        clientDataDto1.setSeries("CJ");
        clientDataDto1.setValidTo("2023-10-11");


        clientDataDto2 = new ClientDataDto();
        clientDataDto2.setCNP("1234567892222");
        clientDataDto2.setNumber("123456");
        clientDataDto2.setSeries("KX");
        clientDataDto2.setValidTo("2020-08-11");
    }

    @Test
    public void testFullClientCheck_expiredId() throws Exception {

        Mockito.when(clientExistenceCheckService.checkIfClientExists(clientDataDto2.getCNP(),clientDataDto2.getSeries(), clientDataDto2.getNumber())).thenReturn(Boolean.FALSE);
        Mockito.when(clientReputationService.getClientReputationScore(clientDataDto2.getCNP(),clientDataDto2.getSeries(), clientDataDto2.getNumber())).thenReturn(0);

        String actual = clientEnrollmentService.fullClientCheck(clientDataDto2);

        assertEquals("Expired ID, enrollment not possible!", actual);
    }

    @Test
    public void testFullClientCheck_clientExists() throws Exception {

        Mockito.when(clientExistenceCheckService.checkIfClientExists(clientDataDto1.getCNP(),clientDataDto1.getSeries(), clientDataDto1.getNumber())).thenReturn(Boolean.TRUE);
        Mockito.when(clientReputationService.getClientReputationScore(clientDataDto1.getCNP(),clientDataDto1.getSeries(), clientDataDto1.getNumber())).thenReturn(0);

        String actual = clientEnrollmentService.fullClientCheck(clientDataDto1);

        assertEquals("Client already enrolled!", actual);
    }

    @Test
    public void testFullClientCheck_highRisk() throws Exception {

        Mockito.when(clientExistenceCheckService.checkIfClientExists(clientDataDto1.getCNP(),clientDataDto1.getSeries(), clientDataDto1.getNumber())).thenReturn(Boolean.FALSE);
        Mockito.when(clientReputationService.getClientReputationScore(clientDataDto1.getCNP(),clientDataDto1.getSeries(), clientDataDto1.getNumber())).thenReturn(150);

        String actual = clientEnrollmentService.fullClientCheck(clientDataDto1);

        assertEquals("Risky candidate, enrollment not acceptable", actual);
    }

    @Test
    public void testFullClientCheck_acceptance() throws Exception {

        Mockito.when(clientExistenceCheckService.checkIfClientExists(clientDataDto1.getCNP(),clientDataDto1.getSeries(), clientDataDto1.getNumber())).thenReturn(Boolean.FALSE);
        Mockito.when(clientReputationService.getClientReputationScore(clientDataDto1.getCNP(),clientDataDto1.getSeries(), clientDataDto1.getNumber())).thenReturn(12);

        String actual = clientEnrollmentService.fullClientCheck(clientDataDto1);

        assertEquals("Candidate with no risk", actual);
    }

}
