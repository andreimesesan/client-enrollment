# The Challenge

Design, document and partial implement an API that would service a front-end application which enrolls new clients.

## Description

Suppose there is a frontend application whose purpose is to enroll new clients into the core banking system. The Frontdesk Employee uses the application
to input clients data (identity document information) then submit a full Client check. Based on the checking response, the Frontdesk Employee will either generate 
the enrolment document or a denial document specifying the reason the enrolment was denied. In both cases the generated document will have to be signed by both Client
and Frontdesk Employee and submitted back to system.

## Tasks

1. Design (create endpoint contracts) the API that will support the above operations (full client check, generating enrolment/denial documents, etc),
create the needed documentation for the API 
2. Implement the operation that performs the Client check.

The Client check operation should check the document id validity (whether it's expired or not), query an external system for checking client's reputation (the client reputation
is defined by a number: 0 to 20 means 'Candidate with no risk', 21 to 99 means 'Candidate with medium risk, but enrollment still possible', anything above 99 means 'Risky candidate,
enrollment not acceptable')
and querying another external system to check whether the client already exists.

## Asumptions:

- The two external systems already exist
- Contract between systems has been agreed, this contains the ClientSearchCriteria
- The rejection/ acceptance documents will be generated at a later date
- The external systems do not have API security, simle HTTP Requests are used

## Solution
- Rest Controller for the 3 endpoints(client check, generating acceptance document, generating rejection document)
- Services which call a generic connector for the client score and existence check
- Factory Method design pattern used for Documents


API Documentation done using Swagger 2 and Javadoc comments

## Technology stack
- Maven
- Java 1.8
- Spring Boot 2.3.3
- Lombok
- Jackson 2.11.2
- Swagger 2

## Project setup

### Prerequisties
- Java version 1.8
- Maven version 3
- Your favourite Java IDE

### Intellij Setup:
- File-> Open-> Navivigate to projectdir -> click on pom.xml-> chose open as project
- If dependencies are not automatically downloaded, go to Terminal tab from Intellij, and run "mvn clean install -DskipTests" and then "mvn clean install"

In order to see the Swagger page:
- Run project
- Go to "/swagger-ui.html"